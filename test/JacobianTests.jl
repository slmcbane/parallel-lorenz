@testset "JacobianTests" begin

@testset "ForwardTests" for i = 1:100
    @test begin
        α = randn()
        β = randn()
        u = randn(K*(J+1))
        v = randn(K*(J+1))
        j1 = StepJacobian(undef, 0.001)

        rk_step_jacobian!(j1, similar(u), u, DEFAULT_PARAMS, 0.001)

        j2 = ForwardDiff.jacobian!(similar(u*u'),
            (dest, state) -> rk_step!(dest, state, DEFAULT_PARAMS, 0.001),
            similar(u), u)

        prod1 = α * j2 * u + β * v
        mul!(v, j1, u, α, β) ≈ prod1
    end
end

@testset "AdjointTests" for i = 1:100
    @test begin
        α = randn()
        β = randn()
        u = randn(K*(J+1))
        v = randn(K*(J+1))
        j1 = StepJacobian(undef, 0.001)

        rk_step_jacobian!(j1, similar(u), u, DEFAULT_PARAMS, 0.001)

        j2 = ForwardDiff.jacobian!(similar(u*u'),
            (dest, state) -> rk_step!(dest, state, DEFAULT_PARAMS, 0.001),
            similar(u), u)

        prod1 = α * transpose(j2) * u + β * v
        mul!(v, transpose(j1), u, α, β) ≈ prod1
    end
end

end
