const TEST_SOLUTION_FILENAME = "test_solutions.dat"

const tspans = [(0.0, 10.0), (1.0, 3.0), (5.0, 10.0)]
const Δts = [0.001, 0.002, 0.003]

@testset "Serialization Tests" for tspan in tspans
    for Δt in Δts
        @test begin
            u₀ = randn(K*(J+1))
            sol1 = serial_forward_solve( u₀, tspan, Δt )
            serialize_solution(TEST_SOLUTION_FILENAME, sol1)
            sol2 = deserialize_solution(TEST_SOLUTION_FILENAME)
            trajectory_differences = sol1[1].trajectory .== sol2[1].trajectory
            time_differences = sol1[1].time_points .== sol2[1].time_points
            reduce((a, b) -> a && b, trajectory_differences) &&
            reduce((a, b) -> a && b, time_differences)
        end
    end
end

run(`rm $TEST_SOLUTION_FILENAME`)
