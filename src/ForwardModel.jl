"""
Functionality related to solving the model equations in the forward sweep. Has
an interface intended to be used in a parallel programming model; for serial
computation it's unnecessarily complicated.
"""
module ForwardModel

export init_slices!, num_slices, get_slice, invalidate_slices, TangentLinear,
       SliceSolution, solve_forward!, solve_all!, slices_initialized

using ..TimeStepping, ..LorenzCore, ...Lorenz
using LinearAlgebra

import Base: eltype

"A thin wrapper around a `Vector{StepJacobian}` for code clarity"
struct TangentLinear
    step_jacobians::Vector{StepJacobian}
end

eltype(::TangentLinear) = FLOAT

"""
Data structure holding the model solution for a time slice (t₀, t₁).

Data members
============
- `time_points::Vector`: The times corresponding to the stored model solutions.
- `trajectory::Vector{Vector}`: Holds the model state at each time specified in `time_points`.
- `tangent_linear::TangentLinear`: May hold an empty vector; otherwise, it holds the Jacobian of the timestep operator at each timestep along `time_points` for use in the adjoint model run.
"""
struct SliceSolution
    time_points::Vector{FLOAT}
    trajectory::Vector{Vector{FLOAT}}
    tangent_linear::TangentLinear
end

const MY_SLICES = SliceSolution[]

num_slices() = length(MY_SLICES)
get_slice(indx) = MY_SLICES[indx]
slices_initialized() = num_slices() > 0

"""
`init_slices!(time_slices::Vector{Tuple{FLOAT, FLOAT}}, Δt::FLOAT)`

Initialize this processes global structure to solve the model over the given
time slices with given timestep. `time_slices` is assumed to already be sorted,
and the last element of slice i should match the first element of slice i+1.
This does not set an initial condition.
"""
function init_slices!(time_slices::Vector{Tuple{FLOAT, FLOAT}}, Δt::FLOAT)
    resize!(MY_SLICES, length(time_slices))
    for i = 1:length(time_slices)
        slice = time_slices[i]
        MY_SLICES[i] = SliceSolution(
            collect(slice[1]:Δt:slice[2]), Vector{FLOAT}[],
            TangentLinear(StepJacobian[])
        )

        if MY_SLICES[i].time_points[end] != slice[2]
            push!(MY_SLICES[i].time_points, slice[2])
        end
        resize!(MY_SLICES[i].trajectory, length(MY_SLICES[i].time_points))
        resize!(MY_SLICES[i].tangent_linear.step_jacobians, length(MY_SLICES[i].time_points) - 1)
        for j = 1:length(MY_SLICES[i].trajectory)
            MY_SLICES[i].trajectory[j] = Vector{FLOAT}(undef, K*(J+1))
            if j < length(MY_SLICES[i].trajectory)
                MY_SLICES[i].tangent_linear.step_jacobians[j] =
                    StepJacobian(undef, MY_SLICES[i].time_points[j+1] - MY_SLICES[i].time_points[j])
            end
        end
    end
end

"""
`invalidate_slices() where T`

Manually set this processes time slices to uninitialize so that they must be
initialized before another model run.
"""
invalidate_slices() = empty!(MY_SLICES)

"""
`solve_slice!(slice::SliceSolution, params)`

Solve the forward model and compute the tangent linear model over the given slice
(must have been initialized with storage for the trajectory allocated and time
points specified), modifying the members of the `SliceSolution` passed.
"""
function solve_slice!(slice::SliceSolution, params)
    @assert length(slice.tangent_linear.step_jacobians) == length(slice.trajectory) - 1
    for i = 1:lastindex(slice.trajectory) - 1
        rk_step_jacobian!( slice.tangent_linear.step_jacobians[i],
                           slice.trajectory[i+1], slice.trajectory[i], params,
                           slice.time_points[i+1] - slice.time_points[i]
                         )
    end
    return slice
end

"""
`solve_slice!(slice::SliceSolution, u₀::Vector{FLOAT}, params)`

Solve the forward model, storing the result in `slice`, but do *not* compute the
tangent linear model (a significant performance gain).
"""
function solve_slice!( slice::SliceSolution, u₀::Vector{FLOAT}, params ) where T
    empty!(slice.tangent_linear.step_jacobians)
    slice.trajectory[1] = u₀
    for i = 1:lastindex(slice.trajectory) - 1
        rk_step!( slice.trajectory[i+1], slice.trajectory[i], params,
                  slice.time_points[i+1] - slice.time_points[i] )
    end
    return slice
end

function solve_all!(params)
    @assert slices_initialized()
    for slice in MY_SLICES
        solve_slice!(slice, params)
    end
end

"""
`solve_forward!(u₀, params)`

Solve the forward model over all of this processes slices, with initial condition
`u₀`. The tangent linear model is not computed. Used for the serial solution of
the equations, since the tangent linear model is not needed.
"""
function solve_forward!(u₀::Vector{FLOAT}, params)
    @assert slices_initialized()
    @assert length(u₀) == K*(J+1)
    solve_slice!(MY_SLICES[1], u₀, params)
    for i = 2:lastindex(MY_SLICES)
        solve_slice!(MY_SLICES[i], MY_SLICES[i-1].trajectory[end], params)
    end
    return MY_SLICES
end

end
