"""
Encapsulating module for Lorenz model experiments. The submodules implement an
adjoint-based parallel-in-time algorithm for solution of the Lorenz system; each
is documented in its own source file.
"""
module Lorenz

const FLOAT = Float64

export FLOAT, pack_params, DEFAULT_PARAMS, K, J

"Pack four parameters into a tuple of `Val`s."
pack_params( F, h, c, b ) = ( Val{F}(), Val{h}(), Val{c}(), Val{b}() )

"""
These are the parameters used for the reference run in Schneider et al; they are
the default when no parameters are supplied.
"""
const DEFAULT_PARAMS = pack_params(10.0, 1.0, 10.0, 10.0)

"The number of slow variables in the model"
const K = 36
"The number of fast variables to each slow variable"
const J = 10

include("LorenzCore.jl")
include("TimeStepping.jl")
include("ForwardModel.jl")
include("GradientComputation.jl")
include("ConjugateGradients.jl")
include("FrontEnd.jl")

end
