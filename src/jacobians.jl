#################################################################################
# The Jacobian of the derivative operator has a fixed sparse structure. I store
# the colptr and rowval vectors used to indicate matrix structure in the
# compressed-sparse-column data structure permanently (per process) in objects
# with a static scope (or that's what it would be called in a compiled language).
# The few functions below build those fixed vectors.
#################################################################################

function push_xcol_coords!(rowvs, k)
    if k == 1
        push!(rowvs, 1)
        for j = 1:J
            push!(rowvs, 1+j)
        end
        push!(rowvs, J+2)
        push!(rowvs, 2*J+3)
        push!(rowvs, xindex(K))
    elseif k == KMINUS
        push!(rowvs, 1)
        push!(rowvs, xindex(KMINUS-1))
        push!(rowvs, xindex(KMINUS))
        for j = 1:J
            push!(rowvs, xindex(KMINUS)+j)
        end
        push!(rowvs, xindex(K))
    elseif k == K
        push!(rowvs, 1)
        push!(rowvs, J+2)
        push!(rowvs, xindex(KMINUS))
        push!(rowvs, xindex(K))
        for j = 1:J
            push!(rowvs, xindex(K)+j)
        end
    else
        push!(rowvs, xindex(k-1))
        push!(rowvs, xindex(k))
        for j = 1:J
            push!(rowvs, xindex(k)+j)
        end
        push!(rowvs, xindex(k+1))
        push!(rowvs, xindex(k+2))
    end
end

function push_ycol_coords!(rowvs, j, k)
    indx = xindex(k) + j
    push!(rowvs, xindex(k))
    if j == 1
        push!(rowvs, indx)
        push!(rowvs, indx + 1)
        push!(rowvs, indx + J - 2)
        push!(rowvs, indx + J - 1)
    elseif j == 2
        push!(rowvs, indx-1)
        push!(rowvs, indx)
        push!(rowvs, indx+1)
        push!(rowvs, indx + J - 2)
    elseif j == J
        push!(rowvs, indx - J + 1)
        push!(rowvs, indx-2)
        push!(rowvs, indx-1)
        push!(rowvs, indx)
    else
        push!(rowvs, indx-2)
        push!(rowvs, indx-1)
        push!(rowvs, indx)
        push!(rowvs, indx+1)
    end
end

const jacobian_structure = begin
    colptr = Int[1]
    rowvs  = Int[]
    for k = 1:K
        push_xcol_coords!(rowvs, k)
        push!(colptr, length(rowvs)+1)
        for j = 1:J
            push_ycol_coords!(rowvs, j, k)
            push!(colptr, length(rowvs)+1)
        end
    end
    (colptr, rowvs)
end

"The number of non-zero values in the Jacobian of the time derivative operator"
const jacobian_nnz = (4 + J)*K + K*J*5

@doc """
`Rjac!(nzval, state, params)` computes the Jacobian of the time derivative
operator and returns it as a `SparseMatrixCSC`.

Inputs
======
- `nzval::Vector{T}`: A pre-allocated vector that will hold the non-zeroes in
the matrix returned. It can't be re-used if the sparse matrix is to be
persistent. Must have size `jacobian_nnz`.
- `state::Vector{T}`: The state vector.
- `params`: Expects a tuple of vals, as in input to time derivative operator.
""" Rjac!

# The @eval trick here results in the *result* of the call to
# `initialize_jacobian_storage` being interpolated into the function body, so
# that the arrays are only allocated once when the code is evaluated the first
# time. The sparse structure is re-used in this fashion.
function Rjac!( nzval::Vector{T}, state::Vector{T}, params ) where T
    colptr, rowvs = jacobian_structure
    if SANITY_CHECKS_ON
        @assert length(nzval) == length(rowvs)
    end
    return Rjac_impl!( nzval, colptr, rowvs, state, params )
end

function Rjac_impl!( nzval, colptr, rowvs, state,
                     params::Tuple{Val{F}, Val{h}, Val{c}, Val{b}}
                   ) where {F, h, c, b}
    i = 1
    i = jacobian_x_column!( i, nzval, state, Val{1}(), params )
    i = jacobian_y_columns!( i, nzval, state, 1, params )
    i = jacobian_x_column!( i, nzval, state, Val{2}(), params )
    i = jacobian_y_columns!( i, nzval, state, 2, params )

    for k = 3:K-2
        i = jacobian_x_column!( i, nzval, state, k, params )
        i = jacobian_y_columns!( i, nzval, state, k, params )
    end

    i = jacobian_x_column!( i, nzval, state, Val{KMINUS}(), params )
    i = jacobian_y_columns!( i, nzval, state, KMINUS, params )
    i = jacobian_x_column!( i, nzval, state, Val{K}(), params )
    i = jacobian_y_columns!( i, nzval, state, K, params )

    if SANITY_CHECKS_ON
        @assert i == colptr[end]
    end
    return SparseMatrixCSC(K*(J+1), K*(J+1), colptr, rowvs, nzval)
end

function jacobian_x_column!( i, nzval, state, k,
                             params::Tuple{Val{F}, Val{h}, Val{c}, Val{b}}
                           ) where {F, h, c, b}
    nzval[i] = state[xindex(k-2)]
    nzval[i+1] = -one(eltype(nzval))

    for j = 1:J
        nzval[i+1+j] = c*h / J
    end

    nzval[i+J+2] = state[xindex(k+2)] - state[xindex(k-1)]
    nzval[i+J+3] = -state[xindex(k+1)]
    return i + J + 4
end

function jacobian_x_column!( i, nzval, state, k::Val{1},
                             params::Tuple{Val{F}, Val{h}, Val{c}, Val{b}}
                           ) where {F, h, c, b}
    nzval[i] = -one(eltype(nzval))

    for j = 1:J
        nzval[i+j] = c*h / J
    end

    nzval[i+J+1] = state[2*J + 3] - state[xindex(K)]
    nzval[i+J+2] = -state[J+2]
    nzval[i+J+3] = state[xindex(K-1)]
    return i + J + 4
end

function jacobian_x_column!( i, nzval, state, k::Val{2},
                             params::Tuple{Val{F}, Val{h}, Val{c}, Val{b}}
                           ) where {F, h, c, b}
    nzval[i] = state[xindex(K)]
    nzval[i+1] = -one(eltype(nzval))

    for j = 1:J
        nzval[i+j+1] = c*h / J
    end

    nzval[i+J+2] = state[3*J + 4] - state[1]
    nzval[i+J+3] = -state[2*J + 3]
    return i + J + 4
end

function jacobian_x_column!( i, nzval, state, k::Val{KMINUS},
                             params::Tuple{Val{F}, Val{h}, Val{c}, Val{b}}
                           ) where {F, h, c, b}
    nzval[i] = -state[xindex(K)]
    nzval[i+1] = state[xindex(K-3)]
    nzval[i+2] = -one(eltype(nzval))

    for j = 1:J
        nzval[i+2+j] = c*h / J
    end

    nzval[i+3+J] = state[1] - state[xindex(K-2)]
    return i + J + 4
end

function jacobian_x_column!( i, nzval, state, k::Val{K},
                             params::Tuple{Val{F}, Val{h}, Val{c}, Val{b}}
                           ) where {F, h, c, b}
    nzval[i] = state[J + 2] - state[xindex(K-1)]
    nzval[i+1] = -state[1]
    nzval[i+2] = state[xindex(K-2)]
    nzval[i+3] = -one(eltype(nzval))
    for j = 1:J
        nzval[i+3+j] = c*h / J
    end
    return i + J + 4
end

function jacobian_y_columns!( i, nzval, state, k,
                              params::Tuple{Val{F}, Val{h}, Val{c}, Val{b}}
                            ) where {F, h, c, b}
    i = jacobian_y_column!( i, nzval, state, Val{1}(), k, params )
    i = jacobian_y_column!( i, nzval, state, Val{2}(), k, params )
    for j = 3:J-2
        i = jacobian_y_column!( i, nzval, state, j, k, params )
    end
    i = jacobian_y_column!( i, nzval, state, Val{JMINUS}(), k, params )
    i = jacobian_y_column!( i, nzval, state, Val{J}(), k, params )
    return i
end

function jacobian_y_column!( i, nzval, state, j, k,
                             params::Tuple{Val{F}, Val{h}, Val{c}, Val{b}}
                           ) where {F, h, c, b}
    indx = xindex(k) + j
    nzval[i] = -h*c / J
    nzval[i+1] = -b*c*state[indx-1]
    nzval[i+2] = b*c*(state[indx-2] - state[indx+1])
    nzval[i+3] = -c
    nzval[i+4] = b*c*state[indx+2]
    return i + 5
end

function jacobian_y_column!( i, nzval, state, j::Val{1}, k,
                             params::Tuple{Val{F}, Val{h}, Val{c}, Val{b}}
                           ) where {F, h, c, b}
    indx = xindex(k) + 1
    nzval[i] = -h*c / J
    nzval[i+1] = -c
    nzval[i+2] = b*c*state[indx + 2]
    nzval[i+3] = -b*c*state[indx + J - 1]
    nzval[i+4] = b*c*(state[indx + J - 2] - state[indx + 1])
    return i + 5
end

function jacobian_y_column!( i, nzval, state, j::Val{2}, k,
                             params::Tuple{Val{F}, Val{h}, Val{c}, Val{b}}
                           ) where {F, h, c, b}
    indx = xindex(k) + 2
    nzval[i] = -h*c / J
    nzval[i+1] = b*c*(state[indx + J - 2] - state[indx+1])
    nzval[i+2] = -c
    nzval[i+3] = b*c*state[indx + 2]
    nzval[i+4] = -b*c*state[indx-1]
    return i + 5
end

function jacobian_y_column!( i, nzval, state, j::Val{JMINUS}, k,
                             params::Tuple{Val{F}, Val{h}, Val{c}, Val{b}}
                           ) where {F, h, c, b}
    indx = xindex(k) + JMINUS
    nzval[i] = -h*c / J
    nzval[i+1] = -b*c*state[indx-1]
    nzval[i+2] = b*c*(state[indx-2] - state[indx+1])
    nzval[i+3] = -c
    nzval[i+4] = b*c*state[indx - J + 2]
    return i + 5
end

function jacobian_y_column!( i, nzval, state, j::Val{J}, k,
                             params::Tuple{Val{F}, Val{h}, Val{c}, Val{b}}
                           ) where {F, h, c, b}
    indx = xindex(k) + J
    nzval[i] = -h*c / J
    nzval[i+1] = b*c*state[indx-J+2]
    nzval[i+2] = -b*c*state[indx-1]
    nzval[i+3] = b*c*(state[indx-2] - state[indx - J + 1])
    nzval[i+4] = -c
    return i + 5
end
