"""
This module implements the solution of HΔu = -∇J using conjugate gradients. The
Hessian action applied is the Gauss-Newton Hessian as in [citation]. These
functions are intended to be called from the master process, and control the
exchange of information on the workers appropriately.
"""
module ConjugateGradients

using ..ForwardModel, ..LorenzCore, ..GradientComputation, ...Lorenz
using LinearAlgebra, Distributed

import LinearAlgebra: mul!

export cg_solve

const solution = Vector{FLOAT}[]
const residual = Vector{FLOAT}[]
const pₖ = Vector{FLOAT}[]
const Hpₖ = Vector{FLOAT}[]
const rhs = Vector{FLOAT}[]
const D = Vector{FLOAT}[]
const U = Vector{FLOAT}[]
const L = Vector{FLOAT}[]

function setup_rhs()
    @sync for worker = 1:nworkers()
        @spawnat workers()[worker] if length(rhs) != num_gradients()
            resize!(rhs, num_gradients())
            for i in eachindex(rhs)
                rhs[i] = -one(FLOAT) * get_gradient(i)
            end
        else
            for i in eachindex(rhs)
                rhs[i] .= -one(FLOAT) .* get_gradient(i)
            end
        end
    end
end

function cg_solve(tol, maxiters=100)
    @info "Entering conjugate gradient solver"
    setup_rhs()
    rhsnorm = rhs_dot_rhs() |> sqrt
    # Allocates everything, set guess to all-zeros
    setup_work_vectors()
    N = total_system_size() # Total number of variables; state size times (M - 1)
    k = 0

    r² = old_r² = r_dot_r()

    while k < maxiters
        k += 1
        compute_Hpₖ()
        αₖ = r² / pₖ_dot_Hpₖ()
        update_solution_and_residual(αₖ)

        old_r² = r²
        r² = r_dot_r()

        resnorm = sqrt(r²)
        if resnorm / rhsnorm <= tol
            @info "    (CG): Iter($k): residual = $(resnorm/rhsnorm); converged to tolerance"
            break
        end
        @info "    (CG): Iter($k): residual = $(resnorm/rhsnorm)"

        βₖ = r² / old_r²
        update_pₖ(βₖ)
    end

    return (sqrt(r²) / rhsnorm, k)
end

function setup_work_vectors()
    @sync for worker = 1:nworkers()
        @spawnat workers()[worker] begin
            for v in ( solution, residual, pₖ, Hpₖ, D, U, L )
                if length(v) != length(rhs)
                    resize!(v, length(rhs))
                    for i in eachindex(v)
                        v[i] = Vector{FLOAT}(undef, K*(J+1))
                    end
                end
            end

            for i in eachindex(solution)
                solution[i] .= rhs[i]
                residual[i] .= rhs[i]
                pₖ[i] .= rhs[i]
            end
        end
    end
end

function total_system_size()
    sum(remotecall_fetch(() ->
        isempty(solution) ? 0 : sum(length(sol) for sol in solution), i)
        for i in workers())
end

function r_dot_r()
    worker_dots = [
        remotecall(() -> isempty(residual) ?
                         zero(FLOAT) :
                         sum(BLAS.dot(r, r) for r in residual),
                    worker)
        for worker in workers()
    ]
    sum(fetch(dp) for dp in worker_dots)
end

function rhs_dot_rhs()
    worker_dots = [
        remotecall(() -> isempty(rhs) ? zero(FLOAT) :
                         sum(BLAS.dot(r, r) for r in rhs),
                    worker)
        for worker in workers()
    ]
    sum(fetch(dp) for dp in worker_dots)
end

function pₖ_dot_Hpₖ()
    worker_dots = [
        remotecall(() -> isempty(pₖ) ?
                         zero(FLOAT) :
                         sum(BLAS.dot(pₖ[i], Hpₖ[i]) for i in eachindex(pₖ)),
                    worker)
        for worker in workers()
    ]
    sum(fetch(dp) for dp in worker_dots)
end

function update_solution_and_residual(α)
    @sync for worker in workers()
        @spawnat worker for i in eachindex(solution)
            BLAS.axpy!(α, pₖ[i], solution[i])
            BLAS.axpy!(-α, Hpₖ[i], residual[i])
        end
    end
end

function update_pₖ(β)
    @sync for worker in workers()
        @spawnat worker for i in eachindex(pₖ)
            BLAS.axpby!(one(FLOAT), residual[i], β, pₖ[i])
        end
    end
end

const next_lower = Vector{FLOAT}(undef, K*(J+1))
const next_pₖ = Vector{FLOAT}(undef, K*(J+1))
function compute_Hpₖ()
    # Data exchange comes first; we need to get the pₖ at the beginning of the
    # next process's region for all processes except the last, and the product
    # of the TLM with the last pₖ from the previous process for all except the
    # first
    @sync for worker = 1:nworkers()-1
        my_next = workers()[worker + 1]
        @spawnat workers()[worker] mul!(next_lower, tangent_linear(num_slices()), pₖ[end])
        @spawnat workers()[worker] next_pₖ .= remotecall_fetch( () -> Lorenz.ConjugateGradients.pₖ[1], my_next )
    end
    @sync for worker = 2:nworkers()
        my_prev = workers()[worker-1]
        @spawnat workers()[worker] L[1] .= remotecall_fetch( () -> Lorenz.ConjugateGradients.next_lower, my_prev )
    end

    # Now all of the workers can proceed wholly independently.
    @sync begin
        @spawnat workers()[1] compute_diag(Val{:first_worker}())
        @spawnat workers()[end] compute_diag(Val{:last_worker}())
        for worker = 2:nworkers()-1
            @spawnat workers()[worker] compute_diag()
        end

        @spawnat workers()[1] compute_upper(Val{:first_worker}())
        @spawnat workers()[end] compute_upper(Val{:last_worker}())
        for worker = 2:nworkers()-1
            @spawnat workers()[worker] compute_upper()
        end

        @spawnat workers()[1] compute_lower(Val{:first_worker}())
        for worker = 2:nworkers()
            @spawnat workers()[worker] compute_lower()
        end
    end

    @sync for worker = 1:nworkers()
        @spawnat workers()[worker] for i in eachindex(Hpₖ)
            Hpₖ[i] .= D[i] .- U[i] .- L[i]
        end
    end
end

const D_tmp = Vector{FLOAT}(undef, K*(J+1))
function compute_diag()
    @assert length(pₖ) .== length(D) .== num_slices()
    for slice = 1:num_slices()
        mul!(D_tmp, tangent_linear(slice), pₖ[slice])
        mul!(D[slice], tangent_linear(slice) |> transpose, D_tmp)
        D[slice] .+= pₖ[slice]
    end
end
function compute_diag(::Val{:first_worker})
    @assert length(pₖ) .== length(D) .== num_slices()-1
    for slice = 2:num_slices()
        mul!(D_tmp, tangent_linear(slice), pₖ[slice-1])
        mul!(D[slice-1], tangent_linear(slice) |> transpose, D_tmp)
        D[slice-1] .+= pₖ[slice-1]
    end
end
function compute_diag(::Val{:last_worker})
    @assert length(pₖ) .== length(D) .== num_slices()
    D[num_slices()] .= pₖ[num_slices()]
    for slice = num_slices()-1:-1:1
        mul!(D_tmp, tangent_linear(slice), pₖ[slice])
        mul!(D[slice], tangent_linear(slice) |> transpose, D_tmp)
        D[slice] .+= pₖ[slice]
    end
end

function compute_upper()
    @assert length(U) .== length(pₖ) .== num_slices()
    mul!(U[num_slices()], tangent_linear(num_slices()) |> transpose, next_pₖ)
    for slice = num_slices()-1:-1:1
        mul!(U[slice], tangent_linear(slice) |> transpose, pₖ[slice+1])
    end
end
function compute_upper(::Val{:last_worker})
    @assert length(U) .== num_slices() .== length(pₖ)
    U[num_slices()] .= 0
    for slice = num_slices()-1:-1:1
        mul!(U[slice], tangent_linear(slice) |> transpose, pₖ[slice+1])
    end
end
function compute_upper(::Val{:first_worker})
    @assert length(U) .== num_slices()-1 .== length(pₖ)
    mul!(U[end], tangent_linear(num_slices()) |> transpose, next_pₖ)
    for slice = num_slices()-1:-1:2
        mul!(U[slice-1], tangent_linear(slice) |> transpose, pₖ[slice])
    end
end

function compute_lower()
    @assert length(L) .== length(pₖ) .== num_slices()
    for slice = 2:num_slices()
        mul!(L[slice], tangent_linear(slice-1), pₖ[slice-1])
    end
end
function compute_lower(::Val{:first_worker})
    @assert length(L) .== length(pₖ) .== num_slices()-1
    L[1] .= 0
    for slice = 3:num_slices()
        mul!(L[slice-1], tangent_linear(slice-1), pₖ[slice-2])
    end
end

const mul_tmp = Vector{FLOAT}(undef, K*(J+1))
function mul!(dest::Vector{FLOAT}, A::TangentLinear, x::Vector{FLOAT})
    targets = (mul_tmp, dest)
    if isodd(length(A.step_jacobians))
        mul_tmp .= x
    else
        dest .= x
    end
    n = length(A.step_jacobians)
    for i = n:-1:1
        mul!(targets[i%2+1], A.step_jacobians[n-i+1], targets[2-i%2], one(FLOAT), zero(FLOAT))
    end
    return dest
end

end # module
