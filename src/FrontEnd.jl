"""
Interface to all model components; these are the entry points that should be
used by a driver program.
"""
module FrontEnd

using ..ForwardModel, ..LorenzCore, ..TimeStepping, ..GradientComputation,
      ..ConjugateGradients, ...Lorenz
using Distributed, LinearAlgebra

export serialize_solution, deserialize_solution, serial_forward_solve,
       decompose_time_domain, distribute_time_slices, set_initial_condition,
       set_guesses, forward_sweep, gradient_sweep, compute_guesses, optimcycle

mutable struct BoolWrapper
    val::Bool
end

const DOMAIN_DISTRIBUTED = BoolWrapper(false)
const INITIAL_CONDITION_SET = BoolWrapper(false)
const GUESSES_UPDATED = BoolWrapper(false)
const FORWARD_SWEEP_DONE = BoolWrapper(false)
const GRADIENT_SWEEP_DONE = BoolWrapper(false)

"""
`serialize_solution(filename::AbstractString, slices::Vector{SliceSolution})`

Store binary representation of a solution to disk. The last point of each time
slice is at the same time as the first point of the next; if the solution is
continuous (i.e. converged) then you can remove these duplicate points with
`deleteat!`. I wanted to preserve these points to dump intermediate discontinuous
solutions for debugging.
"""
function serialize_solution(filename::AbstractString, slices::Vector{SliceSolution})
    open(filename, "w") do io
        write(io, K, J, length(slices))
        for slice in slices
            @assert length(slice.time_points) == length(slice.trajectory)
            write(io, length(slice.time_points))
            for i = 1:length(slice.time_points)
                write(io, slice.time_points[i])
                write(io, slice.trajectory[i])
            end
        end
    end
end

"""
`deserialize_solution(filename::AbstractString)`

Read a binary file written by `serialize_solution` and return a
`Vector{SliceSolution}` with the contents of the file.
"""
function deserialize_solution(filename)
    open(filename, "r") do io
        myK = read(io, Int)
        myJ = read(io, Int)
        @assert K == myK && J == myJ
        nslices = read(io, Int)

        slices = Vector{SliceSolution}(undef, nslices)
        for n = 1:nslices
            slice_len = read(io, Int)
            slices[n] = SliceSolution(
                Vector{FLOAT}(undef, slice_len),
                [Vector{FLOAT}(undef, K*(J+1)) for i = 1:slice_len],
                TangentLinear(StepJacobian[])
            )
            for i = 1:slice_len
                slices[n].time_points[i] = read(io, FLOAT)
                read!(io, slices[n].trajectory[i])
            end
        end
        return slices
    end
end

"""
`serial_forward_solve(u₀, tspan::Tuple{T, T}, Δt, params = DEFAULT_PARAMS)`

Solve the forward model over interval `tspan` with timestep `Δt` and initial
condition `u₀`; this signature initializes the global structures in
the `ForwardModel` module for this timespan, which is an expensive operation.
"""
function serial_forward_solve( u₀, tspan, Δt, params = DEFAULT_PARAMS )
    init_slices!([tspan], Δt)
    return solve_forward!(u₀, params)
end

"""
`serial_forward_solve(u₀, params)`

Solve the forward model using the global structures already initialized in
`ForwardModel` by calling `init_slices!`. Avoids the expensive allocations
incurred by the other signature, if the solution domain is already set up.
"""
function serial_forward_solve( u₀, params = DEFAULT_PARAMS )
    @assert slices_initialized()
    return solve_forward!(u₀, params)
end

"""
`decompose_time_domain(tspan::Tuple{T, T}, nslices)`

Divide the given time span into `nslices` slices for distribution to workers.
"""
function decompose_time_domain(tspan, nslices)
    slice_size = (tspan[2] - tspan[1]) / nslices
    time_slices = Vector{Tuple{FLOAT, FLOAT}}(undef, nslices)
    time_slices[1] = (tspan[1], tspan[1] + slice_size)
    for i = 2:nslices
        time_slices[i] = (time_slices[i-1][2], time_slices[i-1][2] + slice_size)
    end
    time_slices[end] = (time_slices[end][1], tspan[2])
    return time_slices
end

const worker_slices = Int[]

"""
`distribute_time_slices(time_slices::Vector{Tuple{FLOAT, FLOAT}}, Δt)`

Divide the time slices given as equitably as possible among all workers. Calls
`init_slices!` on each worker with the given Δt.
"""
function distribute_time_slices(time_slices, Δt)
    nperworker = length(time_slices) ÷ nworkers()
    nslices = length(time_slices)

    resize!(worker_slices, nworkers())
    fill!(worker_slices, 0)
    indx = 1
    while nslices > 0
        worker_slices[indx] += 1
        nslices -= 1
        indx += 1
        indx > nworkers() && (indx = 1)
    end

    slice_index = 1
    @sync for worker_num = 1:nworkers()
        slices = time_slices[slice_index:slice_index+worker_slices[worker_num]-1]
        @spawnat workers()[worker_num] begin
            init_slices!(slices, Δt)
            DOMAIN_DISTRIBUTED.val = true
        end
        slice_index += worker_slices[worker_num]
    end

    DOMAIN_DISTRIBUTED.val = true
end

function set_initial_condition(u₀)
    @assert length(u₀) == K*(J+1)
    @assert DOMAIN_DISTRIBUTED.val "Call distribute_time_slices to set up the domain"
    remotecall_wait(() -> get_slice(1).trajectory[1] = u₀, workers()[1])
    FORWARD_SWEEP_DONE.val = false
    INITIAL_CONDITION_SET.val = true
end

@eval function compute_guesses(u₀, time_slices::Vector{Tuple{FLOAT, FLOAT}}, Δt, params)
    tmp = $( (Vector{FLOAT}(undef, K*(J+1)), Vector{FLOAT}(undef, K*(J+1))) )
    tmp[1] .= u₀
    guesses = Vector{FLOAT}[]
    for i = 1:lastindex(time_slices)-1
        t = time_slices[i][1]
        j = 1
        while t < time_slices[i][2]
            rk_step!(tmp[j%2 + 1], tmp[2 - j%2], params, Δt)
            j = j%2 + 1
            t += Δt
        end
        push!(guesses, tmp[j] |> deepcopy)
        j == 2 && (tmp[1] .= tmp[2])
    end
    return guesses
end

function set_guesses(guesses)
    @assert DOMAIN_DISTRIBUTED.val "Call distribute_time_slices to set up the domain"
    @assert length(guesses) == sum(worker_slices) - 1
    for guess in guesses
        @assert length(guess) == K*(J+1)
    end

    guess_num = 1
    @sync for worker_num = 1:nworkers()
        if worker_num == 1 && worker_slices[1] > 1
            my_guesses = guesses[1:worker_slices[1]-1]
            @spawnat workers()[1] for j = 1:length(my_guesses)
                get_slice(j+1).trajectory[1] = my_guesses[j]
            end
            guess_num += worker_slices[1] - 1
        elseif worker_num != 1
            my_guesses = guesses[guess_num:worker_slices[worker_num]+guess_num-1]
            @spawnat workers()[worker_num] for j = 1:length(my_guesses)
                get_slice(j).trajectory[1] = my_guesses[j]
            end
            guess_num += worker_slices[worker_num]
        end
    end
    FORWARD_SWEEP_DONE.val = false
    GUESSES_UPDATED.val = true
end

function forward_sweep(params)
    @info "Doing forward solve and computing TLM"
    @assert INITIAL_CONDITION_SET.val && GUESSES_UPDATED.val
    @sync for worker_num = 1:nworkers()
        @spawnat workers()[worker_num] solve_all!(params)
    end
    FORWARD_SWEEP_DONE.val = true
end

function gradient_sweep()
    @info "Entering gradient sweep"
    @assert FORWARD_SWEEP_DONE.val
    first_worker = workers()[1]
    @sync for i = 1:nworkers()
        @spawnat workers()[i] init_misfits(first_worker)
    end

    compute_all_misfits()

    @sync for i = 1:nworkers()
        @spawnat workers()[i] init_adjoints()
    end

    @sync begin
        next = workers()[2]
        @spawnat workers()[1] compute_adjoints(next, Val{:first_worker}())
        for i = 2:nworkers()-1
            next = workers()[i+1]
            @spawnat workers()[i] compute_adjoints(next)
        end
        @spawnat workers()[nworkers()] compute_adjoints(Val{:last_worker}())
    end

    @sync for i = 1:nworkers()
        @spawnat workers()[i] init_gradients()
    end

    @sync for i = 1:nworkers()
        @spawnat workers()[i] compute_gradients()
    end
end

function compute_all_misfits()
    @sync begin
        @spawnat workers()[1] compute_misfits()
        for i = 2:nworkers()
            prev = workers()[i-1]
            @spawnat workers()[i] compute_misfits(prev)
        end
    end
end

function optimcycle(params; cg_tol=0.1, cg_maxiters=100)
    if !FORWARD_SWEEP_DONE.val
        forward_sweep(params)
    end
    gradient_sweep()
    @info "Objective function is $(compute_objective())"
    cg_solve(cg_tol, cg_maxiters)
    update_guesses(params)
end

function update_guesses(params, τ = 2.0, β = 1e-4)
    @info "Entering Armijo line search"
    old_objective = compute_objective()
    stash_old_guesses()
    m = compute_armijo_m()
    m <= zero(FLOAT) || @error "Computed direction is not a descent direction"

    α = one(FLOAT)
    iter = 1
    while true
        @info "    (Armijo): Iteration 1: α = $α"
        compute_new_guesses(α)
        forward_sweep(params)
        compute_all_misfits()
        obj = compute_objective()
        @info "    (Armijo): Old objective - new objective = $(old_objective - obj)"
        if old_objective - obj >= -β * α * m
            @info "    (Armijo): Reached sufficient descent"
            break
        end
        α /= τ
        iter += 1
        α ≈ eps(FLOAT) && @error "Line search failed to find α with descent"
    end
end

const old_guesses = Vector{FLOAT}[]
function stash_old_guesses()
    @sync begin
        @spawnat workers()[1] stash_guesses(Val{:first_worker}())
        for i = 2:nworkers()
            @spawnat workers()[i] stash_guesses()
        end
    end
end

function stash_guesses(::Val{:first_worker})
    if length(old_guesses) != num_slices() - 1
        resize!(old_guesses, num_slices() - 1)
        for i in eachindex(old_guesses)
            old_guesses[i] = get_slice(i+1).trajectory[1] |> deepcopy
        end
    else
        for i in eachindex(old_guesses)
            old_guesses[i] .= get_slice(i+1).trajectory[1]
        end
    end
end
function stash_guesses()
    if length(old_guesses) != num_slices()
        resize!(old_guesses, num_slices())
        for i in eachindex(old_guesses)
            old_guesses[i] = get_slice(i).trajectory[1] |> deepcopy
        end
    else
        for i in eachindex(old_guesses)
            old_guesses[i] .= get_slice(i).trajectory[1]
        end
    end
end

function compute_armijo_m()
    inc², incᵀgrad = sum(
        fetch.(
            [remotecall( () -> begin
                    totals = [zero(FLOAT), zero(FLOAT)]
                    for i = 1:num_gradients()
                        totals[1] += BLAS.dot(ConjugateGradients.solution[i], ConjugateGradients.solution[i])
                        totals[2] += BLAS.dot(ConjugateGradients.solution[i], get_gradient(i))
                    end
                    totals
                end, worker
            ) for worker in workers()]
        )
    )
    return sqrt(inc²) * incᵀgrad
end

function compute_new_guesses(α)
    @sync begin
        @spawnat workers()[1] for i in eachindex(old_guesses)
            get_slice(i+1).trajectory[1] .= old_guesses[i] .+ α .* ConjugateGradients.solution[i]
        end

        for worker = 2:nworkers()
            @spawnat workers()[worker] for i in eachindex(old_guesses)
                get_slice(i).trajectory[1] .= old_guesses[i] .+ α .* ConjugateGradients.solution[i]
            end
        end
    end
end

end
