"Functionality related to actually advancing the model in time"
module TimeStepping

using LinearAlgebra, SparseArrays

import LinearAlgebra: mul!, transpose
import Base.eltype

export rk_step!, rk_step_jacobian!, StepJacobian

const DEBUG = true

using ..LorenzCore
using ...Lorenz

const k1, k2, k3, k4 = ( (Vector{FLOAT}(undef, K*(J+1)) for i = 1:4)..., )

"""
`rk_step!(new_state, curr_state, params, h)`: Perform a 4th-order Runge-Kutta
timestep with step size `h`, updating the vector `new_state` with the state at
the next timestep given `curr_state` and the model parameters.
"""
function rk_step_impl!(new_state, curr_state, params, h, k1, k2, k3, k4)
    if DEBUG
        @assert size(new_state) == size(curr_state)
        @assert size(curr_state) == size(k1)
        @assert size(curr_state) == size(k2)
        @assert size(curr_state) == size(k3)
        @assert size(curr_state) == size(k4)
    end

    R(k1, curr_state, params)

    @inbounds for i = 1:lastindex(curr_state)
        new_state[i] = curr_state[i] + 0.5*h*k1[i]
    end

    R(k2, new_state, params)

    @inbounds for i = 1:lastindex(curr_state)
        new_state[i] = curr_state[i] + 0.5*h*k2[i]
    end

    R(k3, new_state, params)

    @inbounds for i = 1:lastindex(curr_state)
        new_state[i] = curr_state[i] + h * k3[i]
    end

    R(k4, new_state, params)

    @inbounds for i = 1:lastindex(curr_state)
        new_state[i] = curr_state[i] +
            h * (k1[i] + 2.0*k2[i] + 2.0*k3[i] + k4[i]) / 6.0
    end

    return new_state
end

rk_step!(new_state::Vector{FLOAT}, curr_state::Vector{FLOAT}, params, h) =
    rk_step_impl!(new_state, curr_state, params, h, k1, k2, k3, k4)

function rk_step!(new_state, curr_state, params, h)
    k1, k2, k3, k4 = ( (similar(curr_state) for i = 1:4)..., )
    rk_step_impl!(new_state, curr_state, params, h, k1, k2, k3, k4)
end

#################################################################################
# These types and functions implement the Runge Kutta step with computation of
# the tangent linear model. The `StepJacobian` type encapsulates the information
# needed to compute the TLM's action on a vector, and Base linalg functions have
# methods added to perform that computation.
#################################################################################

"""
Data structure to hold a sparse representation of the Jacobian of the operator
that takes a state to the next state through a Runge-Kutta timestep. The full
Jacobian is about 20% dense, whereas the four intermediate results stored in
this structure are about 98.5% sparse; the overhead of forming the full
Jacobian is much greater than the extra work that has to be done in the form of
sparse matrix-vector products in order to apply its action through this
structure (not to mention the memory savings).
"""
struct StepJacobian
    h :: FLOAT
    J :: NTuple{4, SparseMatrixCSC{FLOAT, Int}}
end

function StepJacobian(u::UndefInitializer, h::FLOAT)
    return StepJacobian( h,
        ( (SparseMatrixCSC(K*(J+1), K*(J+1), jacobian_structure...,
           Vector{FLOAT}(undef, jacobian_nnz)) for i = 1:4)..., )
    )
end

eltype(s::StepJacobian) = FLOAT

transpose(J::StepJacobian) = Transpose{FLOAT, StepJacobian}(J)

const mul_tmps = ( (Vector{FLOAT}(undef, K*(J+1)) for i = 1:3)..., )

function mul!(v::Vector{FLOAT}, A::StepJacobian, u::Vector{FLOAT}, α::FLOAT, β::FLOAT)
    if β == zero(FLOAT)
        BLAS.blascopy!(length(u), u, 1, v, 1)
    else
        BLAS.scal!(length(v), β, v, 1)
        BLAS.axpy!(α, u, v)
    end

    mul!(v, A.J[4], u, α * A.h/6, one(FLOAT))

    mul!(mul_tmps[1], A.J[3], u, one(FLOAT), zero(FLOAT))
    mul!(v, A.J[4], mul_tmps[1], α * A.h^2 / 6, one(FLOAT))
    BLAS.axpy!(α * A.h/3, mul_tmps[1], v)

    mul!(mul_tmps[1], A.J[2], u, one(FLOAT), zero(FLOAT))
    mul!(mul_tmps[2], A.J[3], mul_tmps[1], one(FLOAT), zero(FLOAT))
    mul!(v, A.J[4], mul_tmps[2], α * A.h^3 / 12, one(FLOAT))
    BLAS.axpy!(α * A.h^2 / 6, mul_tmps[2], v)
    BLAS.axpy!(α * A.h / 3, mul_tmps[1], v)

    mul!(mul_tmps[1], A.J[1], u, one(FLOAT), zero(FLOAT))
    mul!(mul_tmps[2], A.J[2], mul_tmps[1], one(FLOAT), zero(FLOAT))
    mul!(mul_tmps[3], A.J[3], mul_tmps[2], one(FLOAT), zero(FLOAT))
    mul!(v, A.J[4], mul_tmps[3], α * A.h^4 / 24, one(FLOAT))
    BLAS.axpy!(α * A.h^3 / 12, mul_tmps[3], v)
    BLAS.axpy!(α * A.h^2 / 6, mul_tmps[2], v)
    BLAS.axpy!(α * A.h / 6, mul_tmps[1], v)

    return v
end

function mul!(v::Vector{FLOAT}, t::Transpose{FLOAT, StepJacobian}, u::Vector{FLOAT}, α::FLOAT, β::FLOAT)
    if β == zero(β)
        BLAS.blascopy!(length(u), u, 1, v, 1)
    else
        BLAS.scal!(length(v), β, v, 1)
        BLAS.axpy!(α, u, v)
    end

    A = t.parent

    mul!(v, transpose(A.J[1]), u, α * A.h / 6, one(FLOAT))

    mul!(mul_tmps[1], transpose(A.J[2]), u, one(FLOAT), zero(FLOAT))
    mul!(v, transpose(A.J[1]), mul_tmps[1], α * A.h^2 / 6, one(FLOAT))
    BLAS.axpy!(α * A.h / 3, mul_tmps[1], v)

    mul!(mul_tmps[1], transpose(A.J[3]), u, one(FLOAT), zero(FLOAT))
    mul!(mul_tmps[2], transpose(A.J[2]), mul_tmps[1], one(FLOAT), zero(FLOAT))
    mul!(v, transpose(A.J[1]), mul_tmps[2], α * A.h^3 / 12, one(FLOAT))
    BLAS.axpy!(α * A.h^2 / 6, mul_tmps[2], v)
    BLAS.axpy!(α * A.h / 3, mul_tmps[1], v)

    mul!(mul_tmps[1], transpose(A.J[4]), u, one(FLOAT), zero(FLOAT))
    mul!(mul_tmps[2], transpose(A.J[3]), mul_tmps[1], one(FLOAT), zero(FLOAT))
    mul!(mul_tmps[3], transpose(A.J[2]), mul_tmps[2], one(FLOAT), zero(FLOAT))
    mul!(v, transpose(A.J[1]), mul_tmps[3], α * A.h^4 / 24, one(FLOAT))
    BLAS.axpy!(α * A.h^3 / 12, mul_tmps[3], v)
    BLAS.axpy!(α * A.h^2 / 6, mul_tmps[2], v)
    BLAS.axpy!(α * A.h / 6, mul_tmps[1], v)

    return v
end

"""
`rk_step_jacobian!(jacobian::StepJacobian, new_state, curr_state, params, h)`

Performs the Runge-Kutta step, but additionally computes the Jacobian of the
operator evaluated at `curr_state` and stores it in the provided `StepJacobian`.
"""
function rk_step_jacobian!(jacobian, new_state, curr_state, params, h)
    @assert jacobian.h == h
    Rjac!(jacobian.J[1].nzval, curr_state, params)
    R(k1, curr_state, params)

    @inbounds for i = 1:lastindex(curr_state)
        new_state[i] = curr_state[i] + 0.5*h*k1[i]
    end
    Rjac!(jacobian.J[2].nzval, new_state, params)
    R(k2, new_state, params)

    @inbounds for i = 1:lastindex(curr_state)
        new_state[i] = curr_state[i] + 0.5*h*k2[i]
    end
    Rjac!(jacobian.J[3].nzval, new_state, params)
    R(k3, new_state, params)

    @inbounds for i = 1:lastindex(curr_state)
        new_state[i] = curr_state[i] + h * k3[i]
    end
    Rjac!(jacobian.J[4].nzval, new_state, params)
    R(k4, new_state, params)

    @inbounds for j = 1:lastindex(curr_state)
        new_state[j] = curr_state[j] +
            h * (k1[j] + 2.0*k2[j] + 2.0*k3[j] + k4[j]) / 6.0
    end

    return (new_state, jacobian)
end

end
