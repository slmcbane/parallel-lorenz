module GradientComputation

################################################################################
# Every process except for the *first* worker has the same number of misfit,
# adjoint, and gradient vectors as it does slices; they correspond to the first
# time point in each slice.
################################################################################

using ...Lorenz, ..LorenzCore, ..ForwardModel
using Distributed, LinearAlgebra

import LinearAlgebra: mul!, transpose, Transpose

export init_misfits, compute_misfits, init_adjoints, compute_adjoints,
       init_gradients, compute_gradients, get_gradient, num_gradients,
       tangent_linear, compute_objective

const misfits = Vector{FLOAT}[]
const adjoints = Vector{FLOAT}[]
const gradients = Vector{FLOAT}[]

get_gradient(i) = gradients[i]
num_gradients() = length(gradients)
tangent_linear(i) = get_slice(i).tangent_linear

function init_misfits(first_worker)
    n = myid() == first_worker ? num_slices() - 1 : num_slices()
    if length(misfits) != n
        resize!(misfits, n)
        for i in eachindex(misfits)
            misfits[i] = Vector{FLOAT}(undef, K*(J+1))
        end
    end
end

function compute_misfits(prev_worker)
    @assert length(misfits) == num_slices()
    prev_slice_fut = remotecall(() -> get_slice(num_slices()).trajectory[end], prev_worker)
    for slice = num_slices():-1:2
        misfits[slice] .= get_slice(slice).trajectory[1] .- get_slice(slice-1).trajectory[end]
    end
    misfits[1] .= get_slice(1).trajectory[1] .- fetch(prev_slice_fut)
    nothing
end

function compute_misfits()
    @assert length(misfits) == num_slices() - 1
    for slice = 2:num_slices()
        misfits[slice-1] .= get_slice(slice).trajectory[1] .- get_slice(slice-1).trajectory[end]
    end
end

function compute_objective()
    misfits² = [
        remotecall(() -> sum(BLAS.dot(misfit, misfit) for misfit in misfits), worker)
        for worker in workers()
    ]
    return 0.5 * sum(fetch.(misfits²))
end

function init_adjoints()
    if length(adjoints) != length(misfits)
        resize!(adjoints, length(misfits))
        for i in eachindex(adjoints)
            adjoints[i] = Vector{FLOAT}(undef, K*(J+1))
        end
    end
end

function compute_adjoints(next_worker, ::Val{:first_worker})
    @assert length(adjoints) == num_slices() - 1
    next_misfit_fut = remotecall(() -> misfits[1], next_worker)
    for i = 2:num_slices()-1
        mul!(adjoints[i-1], get_slice(i).tangent_linear |> transpose, misfits[i])
    end

    if num_slices() > 1
        mul!(adjoints[num_slices()-1], get_slice(num_slices()).tangent_linear |> transpose, fetch(next_misfit_fut))
    end
    nothing
end

function compute_adjoints(next_worker)
    @assert length(adjoints) == num_slices()
    next_misfit_fut = remotecall(() -> misfits[1], next_worker)
    for i = 1:num_slices()-1
        mul!(adjoints[i], get_slice(i).tangent_linear |> transpose, misfits[i+1])
    end
    mul!(adjoints[num_slices()], get_slice(num_slices()).tangent_linear |> transpose, fetch(next_misfit_fut))
    nothing
end

function compute_adjoints(::Val{:last_worker})
    @assert length(adjoints) == num_slices()
    for i = 1:num_slices() - 1
        mul!(adjoints[i], get_slice(i).tangent_linear |> transpose, misfits[i+1])
    end
    fill!(adjoints[num_slices()], zero(FLOAT))
    nothing
end

function init_gradients()
    @assert length(misfits) == length(adjoints)
    if length(gradients) != length(misfits)
        resize!(gradients, length(misfits))
        for i in eachindex(gradients)
            gradients[i] = Vector{FLOAT}(undef, K*(J+1))
        end
    end
end

function compute_gradients()
    @assert length(gradients) == length(misfits)
    for i in eachindex(gradients)
        gradients[i] .= misfits[i] .- adjoints[i]
    end
end

transpose(t::TangentLinear) = Transpose{FLOAT, TangentLinear}(t)

const mul_tmp = Vector{FLOAT}(undef, K*(J+1))

function mul!(dest::Vector{FLOAT}, A::Transpose{FLOAT, TangentLinear}, x::Vector{FLOAT})
    targets = (mul_tmp, dest)
    if isodd(length(A.parent.step_jacobians))
        mul_tmp .= x
    else
        dest .= x
    end

    for i = length(A.parent.step_jacobians):-1:1
        mul!(targets[i%2 + 1], transpose(A.parent.step_jacobians[i]), targets[2-i%2], one(FLOAT), zero(FLOAT))
    end
    return dest
end

end
