"This module contains the numerical core of the model"
module LorenzCore

using SparseArrays
using ...Lorenz

export K, J, R, Rjac!, DEFAULT_PARAMS, pack_params, xindex, jacobian_nnz, jacobian_structure

# Used to aid type inference and improve performance
const KMINUS = K - 1
const JMINUS = J - 1

# Turn on to enable dimension check in the input parameters to the time
# derivative calculation
const SANITY_CHECKS_ON = true

"""
Compute the time derivative of the state vector given current state. Overwrites
`dest` with the time derivative. Parameters are provided as a tuple of `Val`s as
a performance optimization; this allows the parameters to be treated as compile
time constants.

Arguments:
==========
- `dest::Vector`: Overwritten with the time derivative. Size `K*(J+1)`
- `state::Vector`: The input state; size `K*(J+1)`
- `params::Tuple{Val{F}, Val{h}, Val{c}, Val{b}}`: The four model input
   parameters, as val types.
"""
function R(dest, state, params::Tuple{Val{F}, Val{h}, Val{c}, Val{b}}) where {F, h, c, b}
    if SANITY_CHECKS_ON
        if size(state) != (K*(J+1),)
            error("State array has incorrect dimension")
        end
        if size(dest) != size(state)
            error("Size of destination array not the same as size of state array")
        end
    end

    dest[1] = xderivative( state, Val{1}(), params )
    yderivatives!( dest, state, 1, params )
    dest[xindex(2)] = xderivative( state, Val{2}(), params )
    yderivatives!( dest, state, 2, params )
    dest[xindex(K)] = xderivative( state, Val{K}(), params )
    yderivatives!( dest, state, K, params )

    for k = 3:K-1
        dest[xindex(k)] = xderivative( state, k, params )
        yderivatives!( dest, state, k, params )
    end

    return dest
end

R( dest, state ) = R( dest, state, DEFAULT_PARAMS )

#################################################################################
# The state vector has a slow variable followed by its J associated fast
# variables, for a total of K*(J+1) variables. This ordering is the best for
# memory latency considerations.
#
# Ex: for J = 10, the first 12 elements of the state vector are
# [ X₁ Y₁₁ Y₂₁ Y₃₁ Y₄₁ Y₅₁ Y₆₁ Y₇₁ Y₈₁ Y₉₁ Y₁₀₁ X₂ ... ]
#################################################################################

"Find the index of the `k`'th slow variable in the state vector"
xindex( k ) = (k-1)*(J+1) + 1

"Average the fast variables for slow variable `k`; uses a numerically favorable method"
function yavg( state, k )
    avg = zero(eltype(state))
    baseindx = xindex(k)
    @inbounds for j = 1:J
         avg += (state[baseindx+j] - avg) / j
    end
    return avg
end

"""
`xderivative(state, k, params)` computes the derivative of the X (slow) variable
number `k`; there are three versions accepting a `Val` as input for the special
cases in which indexing wraps around.
"""
function xderivative( state, k::Val{1},
                      params::Tuple{Val{F}, Val{h}, Val{c}, Val{b}}
                    ) where {F, h, c, b}
    ybar = yavg( state, 1 )
    @inbounds return state[(K-1)*(J+1) + 1] *
        (state[J+2] - state[(K-2)*(J+1) + 1]) - state[1] + F - h * c * ybar
end

function xderivative( state, k::Val{2},
                      params::Tuple{Val{F}, Val{h}, Val{c}, Val{b}}
                    ) where {F, h, c, b}
    ybar = yavg( state, 2 )
    @inbounds return state[1] * (state[2*J+3] - state[(K-1)*(J+1) + 1]) -
        state[J+2] + F - h * c * ybar
end

function xderivative( state, k::Val{K},
                      params::Tuple{Val{F}, Val{h}, Val{c}, Val{b}}
                    ) where {F, h, c, b}
    ybar = yavg( state, K )
    @inbounds return state[(K-2)*(J+1) + 1] *
        (state[1] - state[(K-3)*(J+1) + 1]) - state[(K-1)*(J+1) + 1] +
        F - h * c * ybar
end

function xderivative( state, k,
                      params::Tuple{Val{F}, Val{h}, Val{c}, Val{b}}
                    ) where {F, h, c, b}
    ybar = yavg( state, k )
    @inbounds return state[xindex(k-1)] *
        (state[xindex(k+1)] - state[xindex(k-2)]) - state[xindex(k)] + F -
        h * c * ybar
end

"""
Computes the derivatives of the fast variables coupled to slow variable number
`k`; `target` is the output vector of the same size as `state` where the
time derivatives are to be output.

`yderivatives!(target, state, k)`
"""
function yderivatives!( target, state, k,
                       params::Tuple{Val{F}, Val{h}, Val{c}, Val{b}}
                      ) where {F, h, c, b}
    @inbounds begin
    xk = state[xindex(k)]
    target[xindex(k) + 1] = b*c*state[xindex(k)+2] *
        (state[xindex(k) + J] - state[xindex(k)+3]) - c * state[xindex(k) + 1] +
        c * h * xk / J

    @simd for j = 2:J-2
        target[xindex(k) + j] = b*c*state[xindex(k)+j+1] *
            (state[xindex(k) + j - 1] - state[xindex(k) + j + 2]) -
            c * state[xindex(k) + j] + c * h * xk / J
    end

    target[xindex(k) + J - 1] = b*c*state[xindex(k) + J] *
        (state[xindex(k) + J - 2] - state[xindex(k) + 1]) -
        c * state[xindex(k) + J - 1] + c * h * xk / J

    target[xindex(k) + J] = b*c*state[xindex(k) + 1] *
        (state[xindex(k) + J - 1] - state[xindex(k) + 2]) -
        c * state[xindex(k) + J] + c * h * xk / J
    end
    return target
end

# jacobians.jl implements computation of the jacobian of R.
include("jacobians.jl")

end # module LorenzCore
